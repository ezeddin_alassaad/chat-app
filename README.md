Socket.IO Chat
This is a README file for the Socket.IO Chat project. The project is a simple chat application built using Socket.IO, HTML, and Node.js. It allows real-time communication between multiple clients through a server.

Prerequisites
Before running the project, make sure you have the following prerequisites installed:

Node.js: A JavaScript runtime environment.
Express.js: A web application framework for Node.js.
Socket.IO: A library that enables real-time, bidirectional, and event-based communication between the browser and the server.
Installation
Clone the repository to your local machine:

bash
Copy
git clone https://github.com/Ezeddin-Alassaad/chat-app.git
Navigate to the project directory:

bash
Copy
cd socket-io-chat
Install the required dependencies:

bash
Copy
npm install
Usage
Start the server:

bash
Copy
node index.js
The server will start running and listen on port 3000.

Open your browser and navigate to http://localhost:3000.

The chat application will be displayed, and you can start sending and receiving messages.

Enter a message in the input field and press "Send" or press Enter to send the message to all connected clients.

If a message starts with a colon (e.g., ":Hello"), it will be displayed in bold on the sender's screen but won't be broadcasted to other users.

To stop the server, press Ctrl + C in the terminal where the server is running.